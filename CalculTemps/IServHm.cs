﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CalculTemps
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IService1" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    public interface IServHm
    {
        [OperationContract]
        Temps MintoHm(int nbMinutes);

        [OperationContract]
        int HmToMin(Temps t);

        // TODO: ajoutez vos opérations de service ici
    }

    // Utilisez un contrat de données comme indiqué dans l'exemple ci-après pour ajouter les types composites aux opérations de service.
    // Vous pouvez ajouter des fichiers XSD au projet. Une fois le projet généré, vous pouvez utiliser directement les types de données qui y sont définis, avec l'espace de noms "CalculTemps.ContractType".
    [DataContract]
    public class Temps
    {
        private int h;
        private int m;


        [DataMember]
        public int H
        {
            get { return h; }
            set { h = value; }
        }

        [DataMember]
        public int M
        {
            get { return m; }
            set { m = value; }
        }
    }
}
