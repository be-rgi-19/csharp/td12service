﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CalculTemps
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "Service1" à la fois dans le code et le fichier de configuration.
    public class ServHm : IServHm, IServAdd
    {
        public Temps MintoHm(int nbMinutes)
        {
            Temps t = new Temps();
            t.H = nbMinutes / 60;
            t.M = nbMinutes % 60;
            return t;
        }

        public int HmToMin(Temps t)
        {
            return t.H * 60 + t.M;
        }

        public Temps AddTemps(Temps t1, Temps t2)
        {
            Temps res  = new Temps();
            res.H = t1.H + t2.H;
            res.M = t1.M + t2.M;
            if (res.M > 59)
            {
                res.M = res.M - 60;
                res.H++;
            }

            return res;
        }
    }
}
